<?php declare(strict_types=1);

namespace App\Tests\Unit\MetricAnalyser;

use App\MetricAnalyser\ChronologicalDataSetValidator;
use App\MetricAnalyser\DatasetSummary;
use App\MetricAnalyser\MetricAnalyser;
use App\MetricAnalyser\UnderperformanceAnalyser;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class MetricAnalyzerTest extends TestCase
{
    /** @var MetricAnalyser */
    private $sut;

    public function setUp()
    {
        $this->sut = new MetricAnalyser(new UnderperformanceAnalyser(), new ChronologicalDataSetValidator());
    }

    public function testAnalyzingWithUnderperformance()
    {
        $dates = ['2019-01-01', '2019-01-02', '2019-01-03'];
        $values = [4., 0., 5.];
        $output = $this->sut->analyse($dates, $values);
        $expected = new DatasetSummary(
            '2019-01-01',
            '2019-01-03',
            3, 0, 5, 4,
            '2019-01-02',
            '2019-01-02'
        );
        $this->assertEquals($expected, $output);
    }

    public function testAnalyzingWithout()
    {
        $dates = ['2019-01-01', '2019-01-02', '2019-01-03'];
        $values = [5., 6., 7.];
        $output = $this->sut->analyse($dates, $values);
        $expected = new DatasetSummary(
            '2019-01-01',
            '2019-01-03',
            6, 5, 7, 6,
            null,
            null
        );
        $this->assertEquals($expected, $output);
    }
}