<?php declare(strict_types=1);

namespace App\Tests\Unit\MetricAnalyser;

use App\MetricAnalyser\ChronologicalDataSetValidator;
use PHPUnit\Framework\TestCase;

class ChronologicalDataSetTest extends TestCase
{
    /** @var ChronologicalDataSetValidator */
    private $sut;

    public function setUp()
    {
        $this->sut = new ChronologicalDataSetValidator();
    }

    /**
     * @doesNotPerformAssertions
     */
    public function testOk()
    {
        $values = [1., 2.];
        $dates = ['2019-01-01', '2019-01-02'];
        $this->sut->validate($values, $dates);
    }

    public function testThrowsIfNotValidDatesGiven()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->sut->validate([1., 2.], ['2019-01-32', '2020-01-01']);
    }

    public function testThrowsIfNotInAscendingOrder()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->sut->validate([1., 2.], ['2019-01-02', '2019-01-01']);
    }

    public function testAcceptsOnlyFloats()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->sut->validate([1, 2.], ['2019-01-01', '2019-01-02']);
    }
}
