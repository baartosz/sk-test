<?php declare(strict_types=1);

namespace App\Tests\Unit\MetricAnalyser;

use App\MetricAnalyser\UnderperformanceAnalyser;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class UnderperformanceAnalyserTest extends TestCase
{
    /** @var UnderperformanceAnalyser */
    private $sut;

    public function setUp()
    {
        $this->sut = new UnderperformanceAnalyser();
    }

    /**
     * @dataProvider getExamples
     * @param null|int[] $expected
     * @param int[] $values
     */
    public function testUnderPerforming($expected, $values)
    {
        $out = $this->sut->getUnderperformanceRange($values);
        $this->assertEquals($expected, $out);
    }

    public function getExamples()
    {
        return [
            [null,   [10, 10, 10]],
            [[1, 2], [10, 1, 1, 10, 10]],
            [[1, 6], [5, 1, 5, 5, 5, 5, 1, 5, 5]],
            [null,   [10, 10, 10, 8, 10]],
            [[1, 1], [10, 2, 10, 8, 10]],
        ];
    }
}