<?php declare(strict_types=1);

namespace App\Tests\Unit\MetricAnalyser;

use App\MetricAnalyser\Median;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class MedianTest extends TestCase
{
    public function testMedian()
    {
        $this->assertEquals(2.5, Median::getMedian([4, 3, 1, 2]));
        $this->assertEquals(3, Median::getMedian([4, 2, 5, 3, 1]));
    }

    public function testThrowsIfEmptyArray()
    {
        $this->expectException(\InvalidArgumentException::class);
        Median::getMedian([]);
    }

    public function testThrowsIfNonNumericInputs()
    {
        $this->expectException(\InvalidArgumentException::class);
        Median::getMedian(['asdasd']);
    }
}