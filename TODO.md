## TODO:
- use DateTime objects if any operations on dates are planned in the future
- use twig to render template if output is supposed to be more complex in the future
- extract file import from AppAnalyseMetricsCommand class to separate class and test it
- handle missing data points
