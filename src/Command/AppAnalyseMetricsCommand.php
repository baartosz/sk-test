<?php declare(strict_types=1);

namespace App\Command;

use App\MetricAnalyser\MetricAnalyser;
use App\MetricAnalyser\ChronologicalDataSetValidator;
use Assert\Assertion;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class AppAnalyseMetricsCommand
 *
 * @package App\Command
 */
class AppAnalyseMetricsCommand extends Command
{
    /** @var MetricAnalyser */
    private $metricAnalyser;

    public function __construct(MetricAnalyser $metricAnalyser)
    {
        parent::__construct();
        $this->metricAnalyser = $metricAnalyser;
    }

    /**
     * @var string
     */
    protected static $defaultName = 'app:analyse-metrics';

    /**
     * Configure the command.
     */
    protected function configure(): void
    {
        $this->setDescription('Analyses the metrics to generate a report.');
        $this->addArgument('input', InputArgument::REQUIRED, 'The location of the test input');
    }

    /**
     * Detect slow-downs in the data and output them to stdout.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $path = $input->getArgument('input');
        list($dates, $values) = $this->importDataSetFromFile($path);

        $summary = $this->metricAnalyser->analyse($dates, $values);

        $output->writeln(sprintf(<<<EOT
SamKnows Metric Analyser v1.0.0
===============================

Period checked:

    From: %s
    To:   %s

Statistics:

    Unit: Megabits per second

    Average: %s
    Min: %s
    Max: %s
    Median: %s
EOT
            ,
            $summary->getPeriodStart(),
            $summary->getPeriodEnd(),
            $this->formatMegabits($summary->getAverage()),
            $this->formatMegabits($summary->getMin()),
            $this->formatMegabits($summary->getMax()),
            $this->formatMegabits($summary->getMedian())
        ));

        if ($summary->getUnderperformanceStart() && $summary->getUnderperformanceEnd()) {
            $output->writeln(sprintf(<<<EOT

Investigate:

    * The period between %s and %s
      was under-performing.

EOT
                ,
                $summary->getUnderperformanceStart(),
                $summary->getUnderperformanceEnd()
            ));
        }
    }

    private function formatMegabits($bytes)
    {
        return round($bytes * 8 / 1000 / 1000, 2);
    }

    private function importDataSetFromFile($path): array
    {
        Assertion::file($path);
        Assertion::readable($path);

        $content = json_decode(file_get_contents($path), true);
        Assertion::isArray($content, sprintf('file %s does not contain valid json', $path));

        $dates = [];
        $values = [];
        foreach ($content['data'][0]['metricData'] as $datum) {
            $dates[] = $datum['dtime'];
            $values[] = $datum['metricValue'];
        }

        return [$dates, $values];
    }
}
