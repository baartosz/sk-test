<?php declare(strict_types=1);

namespace App\MetricAnalyser;

class UnderperformanceAnalyser
{
    /**
     * @param array $values
     * @return null|int[] 2-element array with first and last index of underperformance
     */
    public function getUnderperformanceRange(array $values): ?array
    {
        $median = Median::getMedian($values);

        $selected = [];
        for ($i = 0; $i < count($values); $i++) {
            if ($values[$i] < $median - $median / 3) {
                $selected[] = $i;
            }
        }
        if (count($selected) === 0) {
            return null;
        }

        return [$selected[0], $selected[count($selected) - 1]];
    }
}
