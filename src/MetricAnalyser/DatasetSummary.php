<?php declare(strict_types=1);

namespace App\MetricAnalyser;

class DatasetSummary
{
    /** @var float */
    private $average;
    /** @var float */
    private $min;
    /** @var float */
    private $max;
    /** @var float */
    private $median;
    /** @var string */
    private $underperformanceStart;
    /** @var string */
    private $underperformanceEnd;
    /** @var string */
    private $periodStart;
    /** @var string */
    private $periodEnd;

    public function __construct(
        string $periodStart,
        string $periodEnd,
        float $average,
        float $min,
        float $max,
        float $median,
        ?string $underperformanceStart,
        ?string $underperformanceEnd
    )
    {
        $this->periodStart = $periodStart;
        $this->periodEnd = $periodEnd;
        $this->average = $average;
        $this->min = $min;
        $this->max = $max;
        $this->median = $median;
        $this->underperformanceStart = $underperformanceStart;
        $this->underperformanceEnd = $underperformanceEnd;
    }

    public function getAverage(): float
    {
        return $this->average;
    }

    public function getMin(): float
    {
        return $this->min;
    }

    public function getMax(): float
    {
        return $this->max;
    }

    public function getMedian(): float
    {
        return $this->median;
    }

    public function getUnderperformanceStart(): ?string
    {
        return $this->underperformanceStart;
    }

    public function getUnderperformanceEnd(): ?string
    {
        return $this->underperformanceEnd;
    }

    public function getPeriodStart(): string
    {
        return $this->periodStart;
    }

    public function getPeriodEnd(): string
    {
        return $this->periodEnd;
    }
}