<?php declare(strict_types=1);

namespace App\MetricAnalyser;

use Assert\Assertion;

class ChronologicalDataSetValidator
{
    /**
     * @param float[] $values
     * @param string[] $dates
     */
    public function validate(array $values, array $dates)
    {
        Assertion::eq(count($values), count($dates), 'counts of values don\'t match');

        Assertion::allFloat($values);
        Assertion::allDate($dates, 'Y-m-d');

        $sortedDates = $dates;
        sort($sortedDates);
        Assertion::eq($dates, $sortedDates, 'only chronological set of data is accepted here');
    }
}
