<?php declare(strict_types=1);

namespace App\MetricAnalyser;

class MetricAnalyser
{
    /** @var UnderperformanceAnalyser */
    private $upAnalyser;
    /** @var ChronologicalDataSetValidator */
    private $validator;

    public function __construct(UnderperformanceAnalyser $upAnalyser, ChronologicalDataSetValidator $validator)
    {
        $this->upAnalyser = $upAnalyser;
        $this->validator = $validator;
    }

    /**
     * Takes corresponding sets of dates and values and return summary.
     * Input data have to be in chronological order.
     *
     * @param string[] $dates
     * @param int[] $values
     * @return DatasetSummary
     */
    public function analyse(array $dates, array $values): DatasetSummary
    {
        $this->validator->validate($values, $dates);

        $underperformingStart = null;
        $underperformingEnd = null;

        $indexRange = $this->upAnalyser->getUnderperformanceRange($values);
        if ($indexRange !== null) {
            list($startIndex, $endIndex) = $indexRange;
            $underperformingStart = $dates[$startIndex];
            $underperformingEnd = $dates[$endIndex];
        }

        return new DatasetSummary(
            $dates[0],
            $dates[count($dates)-1],
            array_sum($values) / count($values),
            min($values),
            max($values),
            Median::getMedian($values),
            $underperformingStart,
            $underperformingEnd
        );
    }
}
