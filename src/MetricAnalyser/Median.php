<?php declare(strict_types=1);

namespace App\MetricAnalyser;

use Assert\Assertion;

class Median
{
    /**
     * @param array $values
     * @return float
     */
    public static function getMedian(array $values): float
    {
        Assertion::allNumeric($values);
        Assertion::minCount($values, 1);

        sort($values);
        $count = count($values);

        if ($count % 2 == 1) {
            return $values[(int)($count/2)];
        }

        $higherMidIndex = $count / 2;
        return ($values[$higherMidIndex] + $values[$higherMidIndex - 1]) / 2;
    }
}
